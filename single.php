<?php the_post(); ?>
<div class="container">
	<div class="col-md-10 col-md-offset-1 col-sm-12 page__container post">
		<div class="row">
			<?php the_post_thumbnail( 'large', array( 'class' => 'post-image' ) ); ?>
		</div>
		<div class="row">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-10 col-md-offset-1 page__content">
						<?php the_content(); ?>
						<p class="post-back">
							<a href="javascript:history.go(-1)" ><i class="fa fa-arrow-left"></i> Back to news</a>
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

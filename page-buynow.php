<?php
/*
* Template Name: Buy Now
*/
the_post();

$class_hero  = 'radial-blue-gradient';
$text_class  = 'white';
$class_after = '';

$r_title       = get_field('retailers-title');
$r_intro       = get_field('retailers-intro');
$locator_label = get_field('locator-label');
$locator_btn   = get_field('locator-btn');
$buynow = get_field('buynow', 'options');

$modal = [];
$e     = 0;
?>
<!--Header Title-->
<section class="buynow-hero generic-banner y-after">
	<div class="container text-center">
		<div class="max-width-md">
			<h1 class="medium-title">
				<?php echo $r_title; ?>
			</h1>
			<p class="intro">
				<?php echo $r_intro; ?>
			</p>
		</div>
	</div>
</section>

<!--Story-->
<section id="buynow" class="buynow-content pb-5">
	<div class="container">
		<?php
		if ( function_exists( 'yoast_breadcrumb' ) ) {
			yoast_breadcrumb( '<div id="breadcrumbs"><div class="content">', '</div></div>' );
		}
		?>
		<?php
		/* retailers */
		if( have_rows( 'retailers' ) ): ?>
		<div class="retailers max-width-lg centered">
				<ul class="d-md-flex justify-content-center no-padding">
				<?php
					while ( have_rows( 'retailers' ) ) : the_row();
					$logo = get_sub_field( 'logo' );
					$link = get_sub_field( 'link' );
					$id   = get_sub_field( 'id' );
					?>
					<li class="retailers__item flex-md-fill no-style">
						<a id="<?php echo $id; ?>" href="<?php echo $link['url']; ?>" title="<?php echo $link['title']; ?>" target="_blank">
						<div class="logo-wrapper">
							<img src="<?php echo $logo['url']; ?>" class="img-fluid" alt="<?php echo $logo['title']; ?>">
						</div>
						<span class="small-btn btn-primary">
							<?php echo $buynow; ?>
						</span>
						</a>
					</li>
					<?php endwhile;  ?>
				</ul>
			</div>
		<?php endif;
		?>
		<div class="buynow-store">
			<h2 class="h3 secondary-color">
				<?php echo get_field('locator-label'); ?>
			</h2>
	    <div id="map"></div>
	    <?php $jsonstore = \StoreLocator\Data::googleToJson('http://gsx2json.com/api?id=1LzIeBbnh0kQPb_E_lHvMtQGSj9nSzb-PMoefeD0tBYA&sheet=1&columns=false', 'AIzaSyB7w7AhjjiY6ul0aJ2_rXt0vZRpqiq9cX4'); ?>
	    <script id="jsStore" data-api_key="AIzaSyB7w7AhjjiY6ul0aJ2_rXt0vZRpqiq9cX4" data-theme="<?php echo env('WP_THEME') ?>" src="./themes/<?php echo env('WP_THEME') ?>/assets/scripts/store.js"></script>
	    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB7w7AhjjiY6ul0aJ2_rXt0vZRpqiq9cX4&libraries=places&callback=initMap"></script>
		</div>
	</div>
</section>

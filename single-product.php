<?php
$hero    = get_field( 'product_hero' );
$pack    = get_field( 'image_hero' );
$quality = $hero['quality'];
$sizes   = $hero['size-list'];
$related = get_field( 'related_product' );
$new_display = get_field('new_display');

// generic words
$generic_words   = get_field( 'product_words', 'options' );
$pack_size_label = $generic_words['pack_size'];
$mesure_label    = $generic_words['mesure'];
$quality_label   = $generic_words['quality'];
$buy_label       = $generic_words['buy'];
$online_label    = $generic_words['online'];
$faq_title       = $generic_words['why'];
$display_faq = get_field('faq_display');
$new             = get_field( 'new', 'options' );
$discover        = get_field( 'discover', 'options' );
$where_to_buy    = get_field('where_to_buy', 'options');
$link_buy        = get_field('menu-buynow', 'options');
$back_all        = get_field('all_products','options');

if(isset($_SESSION['cat_page'])){
	$_SESSION['cat_page'];
}
?>

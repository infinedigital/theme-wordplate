<?php
/**
 * Roots initial setup and constants
 */
function roots_setup() {
  // Make theme available for translation
  // Community translations can be found at https://github.com/roots/roots-translations
  load_theme_textdomain('roots', get_template_directory() . '/lang');

  // Enable plugins to manage the document title
  // http://codex.wordpress.org/Function_Reference/add_theme_support#Title_Tag
  add_theme_support('title-tag');

  // Register wp_nav_menu() menus
  // http://codex.wordpress.org/Function_Reference/register_nav_menus

  // register_nav_menus(array(
  //   'nav-social' => __('nav-social', 'roots')
  // ));

  // Add post thumbnails
  // http://codex.wordpress.org/Post_Thumbnails
  // http://codex.wordpress.org/Function_Reference/set_post_thumbnail_size
  // http://codex.wordpress.org/Function_Reference/add_image_size
  add_theme_support('post-thumbnails');
  function remove_default_image_sizes( $sizes) {
    unset( $sizes['thumbnail']); /* default: 150x150 */
    unset( $sizes['medium']); /* default: 300x300 */
    unset( $sizes['large']); /* default: 640x640 */


    return $sizes;
  }
  // add_filter('intermediate_image_sizes_advanced', 'remove_default_image_sizes');
  // declare new custom image sizes here
  // add_image_size('mymedium', 300, '', false);

  // Add post formats
  // http://codex.wordpress.org/Post_Formats
  add_theme_support('post-formats', array('aside', 'gallery', 'link', 'image', 'quote', 'video', 'audio'));

  // Add HTML5 markup for captions
  // http://codex.wordpress.org/Function_Reference/add_theme_support#HTML5
  add_theme_support('html5', array('caption', 'comment-form', 'comment-list'));

  // Tell the TinyMCE editor to use a custom stylesheet
  add_editor_style('/assets/css/editor-style.css');
}
add_action('after_setup_theme', 'roots_setup');

/**
 * Register sidebars
 */
function roots_widgets_init() {
  register_sidebar(array(
    'name'          => __('Primary', 'roots'),
    'id'            => 'sidebar-primary',
    'before_widget' => '<div class="widget %1$s %2$s">',
    'after_widget'  => '</div>',
    'before_title'  => '<h3>',
    'after_title'   => '</h3>',
  ));

  register_sidebar(array(
    'name'          => __('Footer', 'roots'),
    'id'            => 'sidebar-footer',
    'before_widget' => '<div class="widget swiper-slide %1$s %2$s">',
    'after_widget'  => '</div>',
    'before_title'  => '<h3>',
    'after_title'   => '</h3>',
  ));
}
add_action('widgets_init', 'roots_widgets_init');
add_image_size('buynow', 354, 212, false);
add_image_size('story-card', 155, 155, false);
add_theme_support('plate-footer-text', 'Thank you for creating with In Fine.');
add_theme_support('plate-disable-api');
add_theme_support('plate-disable-tabs', ['help', 'screen-options']);
add_theme_support('plate-login-logo', sprintf('%s/%s', get_template_directory_uri(), '/assets/images/logo-in_fine.svg'));

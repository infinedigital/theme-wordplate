<?php
/* If product needed */
add_action(
	'init',
	function() {
		$slug_product = 'product';
		$slug_category = 'category';
		register_extended_post_type(
			'product',
			[
				'quick_edit'     => true,
				'featured_image' => 'Product image',
				'show_in_rest'   => true,
				'supports'       => [ 'title', 'thumbnail' ],
				'has_archive'    => true,
				'menu_icon'      => 'dashicons-cart',
				'rewrite'        => [
					'slug'       => $slug_product,
					'with_front' => false,
				],
				'admin_cols'     =>
				[
					'product-category'     => [
						'taxonomy' => 'product-category',
					],
					'product_featured_image' =>
					[
						'featured_image' => 'thumbnail',
						'title'          => 'Product image',
					],
                    'product-tags'      => [
                        'taxonomy'  => 'product-tags',
                    ]
				],
				'admin_filters'  => [
					'genre' => [
						'taxonomy' => 'product-category',
					],
				],
			],
			[
				'singular' => 'Product',
				'plural'   => 'Products',
				'slug'     => 'products',
			]
		);
		register_extended_taxonomy(
			'product-category',
			'product',
			[
				'public' => false,
			]
		);
        register_extended_taxonomy(
            'product-tags',
            'product',
            [
                'public' => false,
            ]
        );
	}
);

<?php
/**
* @package Wordplate
**/


class InitAcfBuyNow{
	public function init()
	{
		acf_add_options_page();
		add_action('init', array($this, 'register_acf'));
	}
	public function register_acf() {
		acf_field_group(
			[
				'title'           => 'Buy now page',
				'fields'          => $this->register_base_fields(),
				'style'           => 'default',
				'location'        => $this->set_location(),
				'position'        => 'acf_after_title',
				'label_placement' => 'top',
				'menu_order'      => 0,
			]
		);
	}
	private function register_base_fields() {
		$base_fields = [
			acf_tab(
				[
					'label' => 'Buy now page',
					'name'  => 'retailers_tab',
				]
			),
			acf_text(
				[
					'name'          => 'retailers-title',
					'label'         => 'Retailers title',
					'instructions'  => 'Title introducing retailers and store locator.',
					'default_value' => '',
				]
			),
			acf_textarea(
				[
					'name'         => 'retailers-intro',
					'label'        => 'Buy now page introduction',
					'instructions' => htmlentities( 'Use <span></span> to set text in pink.' ),
					'rows'         => '4',
				]
			),
			acf_repeater(
				[
					'name'       => 'retailers',
					'label'      => 'Retailers',
					'layout'     => 'block',
					'instructions' => 'Insert the logo, link and google ID tracking if needed.',
					'sub_fields' => [
						acf_image(
							[
								'name'          => 'logo',
								'label'         => 'Logo',
								'instructions'  => 'Please use format 148x80px',
								'return_format' => 'array',
								'required'      => true,
								'wrapper'       => [
									'width' => 33,
								],
							]
						),
						acf_link(
							[
								'name'     => 'link',
								'label'    => 'Retailer link',
								'required' => true,
								'wrapper'  => [
									'width' => 33,
								],
							]
						),
						acf_text(
							[
								'name'    => 'id',
								'label'   => 'Google tag ID',
								'wrapper' => [
									'width' => 33,
								],
							]
						),
					],
				]
			),
			acf_tab(
				[
					'label' => 'Store locator',
					'name'  => 'locator_tab',
				]
			),
			acf_message(
				[
					'name'    => 'howto_message',
					'label'   => 'Message',
					'message' => '<h3>Add a new Store</h3>
					<p>Please edit the <a href="https://docs.google.com/spreadsheets/d/12ZXnDMoOBgWiuSZfEMDASEVifrCq5uCqZtKSRUqkH98/edit#gid=0" target="_blank" >Google Sheet</a></p>
					<p>You can use <a href="https://www.latlong.net/" target="_blank">this website</a> to find latitude and longitude</p>
					',
				]
			),
			acf_text(
				[
					'name'          => 'locator-label',
					'label'         => 'Search form label',
					'instructions'  => 'Translation of: Type your location and find a Pharmacy near you',
					'default_value' => 'Type your location and find a Pharmacy near you',
					'wrapper'       => [
						'width' => 70,
					],
				]
			),
			acf_text(
				[
					'name'          => 'locator-btn',
					'label'         => 'Search button',
					'required'      => true,
					'default_value' => 'Search now',
					'wrapper'       => [
						'width' => 30,
					],
				]
			),
		];
		return $base_fields;
	}
	private function set_location() {
		$location = [
			[
				acf_location( 'page_template', '==', 'page-buynow.php' ),
			],
		];
		return $location;
	}
}
$acf_story = new InitAcfBuyNow();
$acf_story->init();

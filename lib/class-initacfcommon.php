<?php
/**
* Adding ACF to Page Common
* @package infine
**/

class InitAcfCommon {
	public function init()
	{
		acf_add_options_page();
		add_action('init', array($this, 'register_acf'));
		add_action(
			'init', function () {
				remove_post_type_support('page', 'editor');
				remove_post_type_support('page', 'comments');
				remove_post_type_support('page', 'author');
				remove_post_type_support('page', 'revisions');
				remove_post_type_support('page', 'slug');
				remove_post_type_support('page', 'thumbnail');
			}
		);
	}
	public function register_acf() {
		acf_field_group(
			[
				'title'           => 'Reference',
				'fields'          => $this->register_base_fields(),
				'style'           => 'default',
				'location'        => $this->set_location(),
				'position'        => 'normal',
				'label_placement' => 'bottom',
				'menu_order'      => 99,
			]
		);
	}
	private function register_base_fields() {
		$base_fields = [
			acf_wysiwyg(
				[
					'name'         => 'reference',
					'label'        => 'Reference',
					'media_upload' => false,
					'toolbar'      => 'basic',
				]
			),
		];
		return $base_fields;
	}

	private function set_location() {
		$location = [
			[
				acf_location( 'post_type', 'page' ),
			],
			[
				acf_location( 'post_type', 'product' ),
			],
		];
		return $location;
	}
}
$acf_common = new InitAcfCommon();
$acf_common->init();

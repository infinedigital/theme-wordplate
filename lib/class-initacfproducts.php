<?php
/**
* Adding ACF to Page Home
* @package infine
**/

class InitAcfProduct {
	public function init() {
		acf_add_options_page();
		add_action( 'init', array( $this, 'register_acf' ) );
	}
	public function register_acf() {
		acf_field_group(
			[
				'title'           => 'Product',
				'fields'          => $this->register_base_fields(),
				'style'           => 'default',
				'location'        => $this->set_location(),
				'position'        => 'acf_after_title',
				'label_placement' => 'top',
				'menu_order'      => 0,
			]
		);
	}
	private function register_base_fields() {
		$base_fields = [
			acf_tab(
				[
					'label' => 'Header',
					'name'  => 'header_tabs',
				]
			),
			acf_radio(
				[
					'name'          => 'new_display',
					'label'         => '"New" label display',
					'layout'        => 'horizontal',
					'instructions'  => 'Is it a "New" product ? Select "Yes" if you do. Go to the options to upload your label image.',
					'choices'       => [
						'yes' => 'Yes',
						'no'  => 'No, thanks',
					],
					'default_value' => [
						'no',
					],
					'wrapper' => [
						'width' => 50,
					],
				]
			),
			acf_image(
				[
					'name'          => 'image_hero',
					'label'         => 'Pack image',
					'instructions'  => 'Here is the pack image. Do not forget the "Thumbnail" image on the right aside, for the product listing. Please Contact In Fine if you need another image.',
					'return_format' => 'array',
					'required' => true,
					'wrapper' => [
						'width' => 50,
					],
				]
			),
			acf_group(
				[
					'name'         => 'product_hero',
					'label'        => 'Product intro',
					'instructions' => 'Your product content with name, description, ranges, format. Retailers have a tab for it. Generic terms translation in "Options" page.',
					'sub_fields' => [
						acf_textarea(
							[
								'name'        => 'desc',
								'rows'        => '4',
								'label'       => 'Description',
								'new_lines'   => 'br',
								'instructions' => htmlentities('A few lines to introduce your product. Use <strong></strong> to set text in bold.'),
							]
						),
						acf_textarea(
							[
								'name'        => 'short_desc',
								'rows'        => '2',
								'label'       => 'Short description',
								'new_lines'   => 'br',
								'instructions' => htmlentities('This is for the listing product display, just a few words introducing the product.'),
							]
						),
						acf_repeater(
							[
								'name'          => 'quality',
								'label'         => 'Qualities and features',
								'layout'        => 'block',
								'instructions' => htmlentities('Add a row for every quality of feature for this product. Use <strong></strong> to set text in bold.'),
								'wrapper' => [
									'width' => 50,
								],
								'sub_fields'  => [
									acf_text(
										[
											'name'  => 'item',
											'label' => 'Range label',
										]
									),
								],
							]
						),
						acf_repeater(
							[
								'name'         => 'size-list',
								'label'        => 'Format available',
								'layout'       => 'block',
								'instructions' => 'Select the pack size. Translate mesure unity and the "pack size" word in options.',
								'wrapper' => [
									'width' => 50,
								],
								'sub_fields' => [
									acf_select(
										[
											'name'         => 'size',
											'label'        => 'Product size',
											'choices'      => [
												'5'   => '5ml',
												'20'  => '20ml',
												'50'  => '50ml',
												'60'  => '60ml',
												'115' => '115ml',
												'135' => '135ml',
												'200' => '200ml',
												'210' => '210ml',
												'220' => '220ml',
												'300' => '300ml',
												'400' => '400ml',
											]
										]
									),
								],
							]
						),
						acf_text(
							[
								'name' => 'store_text',
								'label' => 'Available at ...',
								'instructions' => htmlentities('Link to the buynow page. Use <span></span> to highlight the text in rose.'),
								'placeholder' => 'Available at the drugstore or supermarket',
								'default_value' => 'available at the drugstore or supermarket',
								'wrapper' => [
									'width' => 50,
								],
							]
						),
						acf_link(
							[
								'name' => 'buynow_link',
								'label' => 'Buy now page',
								'instructions' => 'You need to define a link to the "Buy now page" to display this row.',
								'wrapper' => [
									'width' => 50,
								],
							]
						),
					],
				]
			),
			acf_tab(
				[
					'label' => 'Retailers',
					'name'  => 'retailers_tab',
				]
			),
			acf_repeater(
				[
					'name'       => 'retailers',
					'label'      => 'Retailers',
					'layout'     => 'block',
					'instructions' => 'Custom retailers for this product if needed. Insert the logo, link and google ID tracking if needed.',
					'sub_fields' => [
						acf_image(
							[
								'name'          => 'logo',
								'label'         => 'Logo',
								'instructions'  => 'Please use format 148x80px',
								'return_format' => 'array',
								'required'      => true,
								'wrapper'       => [
									'width' => 33,
								],
							]
						),
						acf_link(
							[
								'name'     => 'link',
								'label'    => 'Retailer link',
								'required' => true,
								'wrapper'  => [
									'width' => 33,
								],
							]
						),
						acf_text(
							[
								'name'    => 'id',
								'label'   => 'Google tag ID',
								'wrapper' => [
									'width' => 33,
								],
							]
						),
					],
				]
			),
			acf_tab(
				[
					'name' => 'related_tab',
					'label' => 'Related products',
				]
			),
			acf_textarea(
				[
					'name' => 'related_title',
					'label' => 'Product title',
					'rows'  => '3',
					'instructions' => 'Translation of: You may also enjoy',
					'placeholder' => 'You may also enjoy',
					'default_value' => 'You may also enjoy',
					'required' => true,
					'wrapper'       => [
						'width' => 50,
					],
				]
			),
			acf_textarea(
				[
					'name' => 'related_desc',
					'label' => 'Product introduction',
					'rows'  => '3',
					'instructions' => 'Introduce your related in a few words',
					'placeholder' => '',
					'default_value' => '',
					'required' => true,
					'wrapper'       => [
						'width' => 50,
					],
				]
			),
			acf_relationship(
				[
					'name'          => 'related_product',
					'label'         => 'Select your products to display',
					'post_type'     => [
						'product',
					],
					'required'      => false,
					'return_format' => 'id',
				]
			),
		];
		return $base_fields;
	}
	private function set_location() {
		$location = [
			[
				acf_location( 'post_type', '==', 'product' ),
			],
		];
		return $location;
	}
}
$acf_product = new InitAcfProduct();
$acf_product->init();

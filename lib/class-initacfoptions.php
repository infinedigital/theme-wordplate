<?php
/**
* Adding ACF to Page Options
* @package infine
**/

class InitAcfOptions {
	public function init() {
		acf_add_options_page();
		add_action( 'init', array( $this, 'register_acf' ) );
	}
	public function register_acf() {
		acf_field_group(
			[
				'title'           => 'Options',
				'fields'          => $this->register_base_fields(),
				'style'           => 'default',
				'location'        => $this->set_location(),
				'position'        => 'normal',
				'label_placement' => 'top',
				'menu_order'      => 0,
			]
		);
        acf_field_group(
            [
                'title'           => 'Other options',
                'fields'          => $this->register_reusable_fields(),
                'style'           => 'default',
                'location'        => $this->set_location(),
                'position'        => 'normal',
                'label_placement' => 'top',
                'menu_order'      => 1,
            ]
        );
	}
	private function register_base_fields() {
		$base_fields = [
			acf_tab(
				[
					'name'  => 'menu',
					'label' => 'Menu navigation',
				]
			),
			acf_repeater(
				[
					'name'  => 'menu-pages',
					'label' => 'Pages links',
					'instructions' => 'Usually links to the pages, displayed in blue. Drag and drop to re-order.',
					'wrapper'      => [
						'width' => 50,
					],
					'sub_fields' => [
						acf_link(
							[
								'name' => 'link',
								'label' => 'Link to page',
								'instructions' => 'use the "Title" attribute for the label displayed in the button',
							]
						),
					],
				]
			),
			acf_link(
				[
					'name'  => 'menu-buynow',
					'label' => 'Buy now link',
					'instructions' => 'Link to the buy now page. The translation of the button is in "multiple used words".',
					'wrapper' => [
						'width' => 50,
					],
				]
			),
			acf_true_false(
				[
					'name'         => 'lang',
					'label'        => 'Multiple Languages',
					'instructions' => 'Do you need a lang switcher',
					'wrapper' => [
						'width' => 50,
					],
				]
			),
			acf_tab(
				[
					'name'  => 'brand_tab',
					'label' => 'About the brand',
				]
			),
			acf_text(
				[
					'name'         => 'brandname',
					'label'        => 'Brand name',
					'instructions' => 'Displayed in alt attribute and other SEO elements',
					'wrapper' => [
						'width' => 100,
					],
				]
			),
			acf_text(
				[
					'name'         => 'brandslogan',
					'label'        => 'Brand slogan',
					'instructions' => 'Displayed in alt attribute and other SEO elements',
					'placeholder'  => '',
					'default_value' => '',
					'wrapper' => [
						'width' => 50,
					],
				]
			),
			acf_image(
				[
					'name' => 'logo_generic',
					'label' => 'Brand Logo',
					'instructions' => 'Logo used in every area of the website and footer.',
					'return_format' => 'array',
					'wrapper' => [
						'width' => 50,
					],
				]
			),
			acf_tab(
				[
					'name'  => 'generic_product_tab',
					'label' => 'Product words',
				]
			),
			acf_text(
				[
					'name'         => 'new',
					'label'        => 'New label',
					'instructions' => 'Label displayed in the listing products and categories. Translation of : New',
					'default_value' => 'New',
					'wrapper' => [
						'width' => 50,
					],
				]
			),
			acf_text(
				[
					'name'         => 'where_to_buy',
					'label'        => 'Where to buy',
					'instructions' => htmlentities('Label "Where to buy" displayed in the single product page. Use <span></span> to set smaller text. Use maximum 2 small words.'),
					'default_value' => htmlentities('<span>Buy</span> now'),
					'placeholder' => htmlentities('<span>Buy</span> now'),
					'wrapper' => [
						'width' => 50,
					],
				]
			),
			acf_group(
				[
					'name'  => 'product_words',
					'label' => 'Product page words',
					'instructions' => 'Displayed in product page. Please contact in Fine if you need more words.',
					'sub_fields' => [
						acf_text(
							[
								'name'         => 'pack_size',
								'label'        => 'Pack size',
								'instructions' => 'Translation of: Pack size',
								'default_value' => 'Pack size',
								'wrapper' => [
									'width' => 50,
								],
							]
						),
						acf_text(
							[
								'name'         => 'mesure',
								'label'        => 'Pack size mesure',
								'instructions' => 'For the pack content, like mg, ml.',
								'default_value' => 'ml',
								'wrapper' => [
									'width' => 50,
								],
							]
						),
						acf_text(
							[
								'name'         => 'buy',
								'label'        => 'Buy: for markets listing',
								'default_value' => 'Buy',
								'placeholder' => 'Buy',
								'wrapper' => [
									'width' => 50,
								],
							]
						),
						acf_text(
							[
								'name'         => 'online',
								'label'        => 'Online: for retailers listing',
								'default_value' => 'Online',
								'placeholder' => 'Online',
								'wrapper' => [
									'width' => 50,
								],
							]
						),
						acf_text(
							[
								'name'         => 'read_faq',
								'label'        => 'Read all the faq',
								'instructions' => 'Links to the faq page. Translation of : Read all the faq',
								'default_value' => 'Read all the FAQs',
								'placeholder' => 'Read all the FAQs',
								'wrapper' => [
									'width' => 50,
								],
							]
						),
						acf_link(
							[
								'name'  => 'link_faq',
								'label' => 'Link to FAQ',
								'wrapper' => [
									'width' => 50,
								],
							]
						),
					],
				]
			),
			acf_tab(
				[
					'name'  => 'generic_tab',
					'label' => 'Multiple used words',
				]
			),
			acf_text(
				[
					'name'         => 'read',
					'label'        => 'Read the infos label',
					'instructions' => 'Translation of : Read the infos',
					'default_value' => 'Read the infos',
					'wrapper' => [
						'width' => 50,
					],
				]
			),
			acf_text(
				[
					'name'         => 'discover',
					'label'        => 'Discover the product label',
					'instructions' => 'Translation of : Discover the product',
					'default_value' => 'Discover the product',
					'wrapper' => [
						'width' => 50,
					],
				]
			),
			acf_text(
				[
					'name'          => 'no_result',
					'label'         => 'No result found',
					'default_value' => 'Sorry, no result found',
					'instructions'  => 'Displayed in every page that features a searchform and the 404.',
					'wrapper' => [
						'width' => 50,
					],
				]
			),
			acf_text(
				[
					'name'          => 'all',
					'label'         => 'Button to display "all"',
					'default_value' => 'All',
					'instructions'  => 'Displayed in the filters buttons',
					'wrapper' => [
						'width' => 50,
					],
				]
			),
			acf_text(
				[
					'name'          => 'all_products',
					'label'         => 'All products label',
					'default_value' => 'All intimate products',
					'instructions'  => 'Displayed in the "Back to all products" button. Translation of: All intimate products',
					'placeholder' => 'All intimate products',
					'wrapper' => [
						'width' => 50,
					],
				]
			),
			acf_text(
				[
					'name'          => 'all_tips',
					'label'         => 'All tips label',
					'default_value' => 'All intimate tips',
					'instructions'  => 'Displayed in the "Back to all tips" button. Translation of: All intimate tips',
					'placeholder' => 'All intimate tips',
					'wrapper' => [
						'width' => 50,
					],
				]
			),
			acf_text(
				[
					'name' => 'back_top',
					'label' => 'Back to top',
					'default_value' => 'Go to top',
					'instructions' => 'Label of the button linking to the top of the page',
				]
			),
			acf_tab(
				[
					'name'  => 'footer_tab',
					'label' => 'Footer general',
				]
			),
			acf_repeater(
				[
					'name'  => 'social-media',
					'label' => 'Social media links',
					'instructions' => 'The social media links for your brand displayed in the footer',
					'sub_fields' => [
						acf_select(
							[
								'label' => 'Social media item',
								'name' => 'social__name',
								'instructions' => 'Select the media related',
								'wrapper' => array(
									'width' => '50',
								),
								'choices' => array(
									'facebook' => 'Facebook',
									'instagram' => 'Instagram',
									'twitter' => 'Twitter',
									'linkedin' => 'LinkedIn',
									'youtube' => 'YouTube',
									'---' => '---',
									'behance' => 'Behance',
									'discord' => 'Discord',
									'dribbble' => 'Dribbble',
									'flickr' => 'Flickr',
									'medium' => 'Medium',
									'pinterest' => 'Pinterest',
									'skype' => 'Skype',
									'tiktok' => 'TikTok',
									'tumblr' => 'Tumblr',
									'twitch' => 'Twitch',
								),
								'default_value' => array(
									0 => 'facebook',
								),
							]
						),
						acf_link(
							[
								'label' => 'Link',
								'name' => 'social__url',
								'instructions' => 'The URL of your page',
								'wrapper' => array(
									'width' => '50',
								),
							]
						),
					],
				]
			),
			acf_group(
				[
					'name' => 'footer',
					'label' => 'Footer text',
					'wrapper' => [
						'width' => 100,
					],
					'instructions' => 'Copyright, address and generic legal mentions',
					'sub_fields' => [
						acf_textarea(
							[
								'name' => 'address',
								'label' => 'Address of the office',
								'new_lines' => 'br',
								'rows' => '4',
								'placeholder' => 'Industrial Zoning De Prijkels - Venecoweg 26 9810 Nazareth - Belgium',
							]
						),
						acf_text(
							[
								'name' => 'copy',
								'label' => 'Copyright generic text',
								'required' => true,
								'default_value' => 'Copywright 2020 PERRIGO - All right reserved',
							]
						),
						acf_wysiwyg(
							[
								'name' => 'footnote',
								'label' => 'Generic footnotes',
								'toolbar' => 'basic',
								'media_upload' => false,
								'instructions' => 'Mentions visible in every page. Use only list and text elements.',
							]
						),
					],
				]
			),
			acf_group(
				[
					'name' => 'legal',
					'label' => 'Cookies and terms',
					'required' => true,
					'instructions' => 'Cookies,terms and conditions labels in the footer',
					'sub_fields' => [
						acf_link(
							[
								'name' => 'cookies',
								'label' => 'Cookies policy',
								'instructions' => 'Link to : Privacy policy | Cookies preferences',
								'wrapper' => [
									'width' => 33,
								],
							]
						),
						acf_link(
							[
								'name' => 'privacy',
								'label' => 'Privacy statement',
								'instructions' => 'Link to : Privacy statement',
								'wrapper' => [
									'width' => 33,
								],
							]
						),
						acf_link(
							[
								'name' => 'terms',
								'label' => 'Terms & Conditions',
								'instructions' => 'Link to : Terms & Conditions',
								'wrapper' => [
									'width' => 33,
								],
							]
						),
					],
				]
			),
			acf_tab(
				[
					'name'  => 'retailers_tab',
					'label' => 'Retailers',
				]
			),
			acf_text(
				[
					'name'          => 'retailers_title',
					'label'         => 'Retailers title',
					'instructions'  => htmlentities( 'Use <strong></strong> to set text in color.' ),
					'default_value' => 'You can find us ... ',
				]
			),
			acf_repeater(
				[
					'name'         => 'retailers',
					'label'        => 'Retailers',
					'layout'       => 'block',
					'instructions' => 'Insert the logo, link and google ID tracking if needed.',
					'sub_fields' => [
						acf_image(
							[
								'name'          => 'logo',
								'label'         => 'Logo',
								'instructions'  => 'Please use format 148x80px',
								'return_format' => 'array',
								'required'      => true,
								'wrapper'       => [
									'width' => 33,
								],
							]
						),
						acf_link(
							[
								'name'     => 'link',
								'label'    => 'Retailer link',
								'required' => true,
								'wrapper'  => [
									'width' => 33,
								],
							]
						),
						acf_text(
							[
								'name'    => 'id',
								'label'   => 'Google tag ID',
								'wrapper' => [
									'width' => 33,
								],
							]
						),
					],
				]
			),
			acf_tab(
				[
					'name'  => '404_tab',
					'label' => '404 page',
				]
			),
			acf_text(
				[
					'name'          => 'title_404',
					'label'         => '404 page title',
					'required'      => true,
					'default_value' => 'The page you are looking for can not be found.',
					'instructions'  => 'Translation of: The page you are looking for can not be found.',
				]
			),
			acf_text(
				[
					'name'          => 'subtitle_404',
					'label'         => '404 question',
					'required'      => true,
					'default_value' => 'While we have you, did you know that',
					'instructions'  => 'Type a few words about this brand.',
				]
			),
			acf_textarea(
				[
					'name'          => 'desc_404',
					'label'         => 'Fun fact',
					'rows'          => '3',
					'instructions'  => 'Second part of your introduction if wanted',
				]
			),
			acf_link(
				[
					'name'          => 'first_link_404',
					'label'         => 'Link to main page',
					'instructions'  => 'Link to the main page you would like to send.',
					'wrapper'       => [
						'width' => 50,
					],
				]
			),
			acf_link(
				[
					'name'          => 'second_link_404',
					'label'         => 'Link to second page',
					'instructions'  => 'Usually link to the homepage',
					'wrapper'       => [
						'width' => 50,
					],
				]
			),
		];
		return $base_fields;
	}
    private function register_reusable_fields() {
        $base_fields = [
            acf_textarea(
                [
                    'name' => 'google_tag',
                    'label' => 'Google tag code',
                    'rows' => '5',
                    'instructions' => 'Insert complete Google Tag Manager code. Please do not add the noscript tag. Comments can be removed. This code goes as high in the <head> of the page as possible.',
                    'default_value' => '<script></script>'
                ]
            ),
            acf_textarea(
                [
                    'name' => 'google_tag_noscript',
                    'label' => 'No script code for Google Tag',
                    'rows' => '2',
                    'instructions' => 'This code goes immediately after the opening <body> tag.',
                    'default_value' => '<noscript></noscript>'
                ]
            ),
        ];
        return $base_fields;
    }
	private function set_location() {
		$location = [
			[
				acf_location( 'options_page', '==', 'acf-options' ),
			],
		];
		return $location;
	}
}
$acf_home = new InitAcfOptions();
$acf_home->init();

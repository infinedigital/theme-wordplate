<?php the_post();//the_content(); ?>
<section class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="max-width-sm centered text-center">
				<h1>
					<?php the_title(); ?>
				</h1>
				<p>
					<?php echo 'Bienvenue sur le thème wordplate in-fine. Voici un aperçu des template customs ainsi que des comportements et class pré-définis à utiliser.'; ?>
				</p>
				<p>
					<?php echo 'Ici le contenu est dans une div max-width-sm. Tailles des div a définir dans wp-core, max-width-xl, max-width-lg ou max-width-md. Les tailles de ces div changent en fonction des breakpoint pour toujours garder une taille inférieure au container, mis à part en mobile.'; ?>
				</p>
			</div>
			<div class="shadowed p-5 mt-5 border-radius">
				<p>
					<?php echo 'Contenu dans une box avec class "shadowed" et radius à définir dans les variables et wp-core. Ensemble des boutons natifs de WordPress. Couleurs à changer dans les variables.scss ainsi que buttons.'; ?>
				</p>
				<button type="button" class="btn-primary">Primary</button>
				<button type="button" class="btn-secondary">Secondary</button>
				<button type="button" class="btn-success">Success</button>
				<button type="button" class="btn-danger">Danger</button>
				<button type="button" class="btn-warning">Warning</button>
				<button type="button" class="btn-info">Info</button>
				<button type="button" class="btn-link">Link</button>
			</div>
		</div>
	</div>
</section>

<!-- 2 cols image with link + text aligned right on desktop and left or center on responsive: define comp in wp-core -->
<section class="container pt-5 pb-5">
	<div class="row align-items-center h-100">
		<div class="col-md-6 text-right-md"> <!-- must be the same as col-size -->
			<h2>
				<?php the_title(); ?>
			</h2>
			<p>
				<?php echo 'Bloc text avec contenu aligné selon le breakpoint défini. Syntaxe : text-right-Breakpoint. Par exemple: text-right-md associé a une col-md-6 alignera le texte à droite jusqu au moment ou la colonne repassera au dessus de l autre.'; ?>
			</p>
			<a href="#" class="btn btn-primary--gradient">Primary gradient Button</a>
		</div>
		<div class="col-md-6">
			<a href="<?php echo get_home_url(); ?>" title="">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/images/model-thumb.jpg" class="img-fluid">
				</a>
		</div>
	</div>
</section>

<section class="container-fluid mt-5">
	<div class="row two-col-cta">
		<div class="col-sm-6 left-col linear-gradient--bg">
			<div class="left-col__wrapper">
				<h1 class="white">
					<small><?php echo 'h1 small text'; ?></small>
					<?php the_title(); ?>
				</h1>
				<h2 class="white">
					<small><?php echo 'h2 small text'; ?></small>
					<?php the_title(); ?>
				</h2>
				<h3 class="white">
					<small><?php echo 'h3 small text'; ?></small>
					<?php the_title(); ?>
				</h3>
			</div>
		</div>
		<div class="col-sm-6 right-col primary-color--bg">
			<div class="right-col__wrapper">
				<h3>
					<?php echo 'Container-fluid + div class two-col-cta'; ?>
				</h3>
				<p>
					<?php echo 'Container-fluid avec deux colonnes pour contenu type call-to-action. Le background de chaque colonne est affecté à la col parente tandis que le text__wrapper fixe la taille maximum du bloc texte en se basant sur la taille d un container classique.'; ?>
				</p>
				<p>
					<?php echo 'Par exemple: container de 1400px = colonne de 700 - 30px (15px de chaque coté pour correspondre aux container et col de bootstrap).'; ?>
				</p>
				<p>
					<?php echo 'Attribuer au text__wrapper un padding pour aérer le contenu par rapport à la col. Le bouton a qui suit un paragraphe possède une margin-top plus élevée.'; ?>
				</p>
				<a href="#" class="btn-white">
					<?php echo 'button link'; ?>
				</a>
			</div>
		</div>
	</div>
</section>

<!-- 2 cols image with link + text centered and reversed with multiple p before <a> -->
<section class="container pt-5 pb-5">
	<div class="row flex-row-reverse align-items-center h-100">
		<div class="col-md-6 cta-block">
			<h2>
				<?php echo 'Reverse flex-row + cta-block'; ?>
			</h2>
			<p>
				<?php echo 'Contenu flex row reverse avec texte + 2 boutons alignés et margin-top sur la liste qui suit un < p > dans une div cta-block'; ?>
				<?php echo 'Typiquement un cta-block contiendra un titre + du contenu p + un ou plusieurs boutons. En fonction, utiliser un < a > ou un < ul >'; ?>
			</p>
			<ul class="list-inline">
				<li class="list-inline-item">
					<a href="#" class="btn-secondary">
						<?php echo 'Secondary Button'; ?>
					</a>
				</li>
				<li class="list-inline-item">
					<a href="#" class="btn-link">
						<?php echo 'Link Button'; ?>
					</a>
				</li>
			</ul>
		</div>
		<div class="col-md-6">
			<a href="<?php echo get_home_url(); ?>" title="">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/images/model-thumb.jpg" alt="" class="img-fluid">
				</a>
		</div>
	</div>
</section>


<section id="sectionmap">
    <div id="map"></div>
    <?php $jsonstore = \StoreLocator\Data::googleToJson('http://gsx2json.com/api?id=12ZXnDMoOBgWiuSZfEMDASEVifrCq5uCqZtKSRUqkH98&sheet=1&columns=false', 'AIzaSyB7w7AhjjiY6ul0aJ2_rXt0vZRpqiq9cX4'); ?>
    <script id="jsStore" data-api_key="AIzaSyB7w7AhjjiY6ul0aJ2_rXt0vZRpqiq9cX4" data-theme="<?php echo env('WP_THEME') ?>" src="./themes/<?php echo env('WP_THEME') ?>/assets/scripts/store.js"></script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB7w7AhjjiY6ul0aJ2_rXt0vZRpqiq9cX4&libraries=places&callback=initMap"></script>
</section>

<!--IE11-Popup-->
<script>
    var $buoop = {required:{e:-1},mobile:false,style:"bottom",api:2020.07 };
    function $buo_f(){
        var e = document.createElement("script");
        e.src = "//browser-update.org/update.min.js";
        document.body.appendChild(e);
    };
    try {document.addEventListener("DOMContentLoaded", $buo_f,false)}
    catch(e){window.attachEvent("onload", $buo_f)}
</script>

<?php
/*
Archive product
*/

$discover       = get_field( 'discover_product', 'options' );
$header_product = get_field( 'header_product', 'options' );
?>

<?php include('section/product-cat-banner.php'); ?>

<section class="archives product-list container-fluid lightblue--bg">
	<div class="container">
		<div class="row align-items-center">
			<div class="col-12">
			</div>
			<?php if ( have_posts() ) :
				while ( have_posts() ) : the_post();
				$image_product = get_field( 'product-image' );
				$image_url     = wp_get_attachment_image_src( $image_product, 'thumbnail', false, array( 'alt' => get_the_title() ) );
					include( 'section/product-vignette.php' );
			endwhile;
			endif;
			?>
		</div>
	</div>
</section>

<?php
get_template_part( 'templates/head' );
?>
<body <?php body_class( '' ); ?>>

	<?php
	do_action( 'get_header' );
	get_template_part( 'templates/header' );
	?>
	<main  id="swup" class="transition-fade">
	<?php include roots_template_path(); ?>
	</main><!-- /.wrap -->
	<?php get_template_part( 'templates/footer' ); ?>

<?php wp_footer(); ?>
</body>
</html>

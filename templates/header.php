<?php
	$logo_brand   = get_field( 'logo_generic', 'options' );
	$brand_name   = get_field('brandname', 'options');
	$buynow_btn   = get_field('menu-buynow', 'options');
	$buynow_label = get_field('buynow' ,'options');
	$lang         = get_field( 'lang', 'options' );
	$active_page = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
?>
<header id="top" class="main-header">
	<div class="container-fluid main-menu">
		<div class="row">
			<div class="logo-wrapper col-10 col-md-11 col-lg-3 col-xl-2">
				<a href="<?php echo get_home_url(); ?>" class="btn-logo" title="<?php echo $brand_name; ?>">
					<img src="<?php echo $logo_brand['url']; ?>" alt="<?php echo $logo_brand['title']; ?>" class="logo img-fluid">
				</a>
			</div>
			<a id="#" class="col-2 col-md-1 btn-nav" alt="<?php echo $brand_name . ' menu'; ?>">
				<i class="fa fa-bars"></i>
				<i class="fa fa-times"></i>
			</a>
			<div class="col-lg-9 col-xl-10" data-aos="fade-in" data-aos-easing="ease-out-quad" data-aos-duration="1500">
				<nav class="menu-wrapper">
					<div class="row">
						<div class="col-lg-8 col-xl-9">
							<?php if (have_rows ('menu-products', 'options') ) : ?>
								<ul class="link-list inline product-links">
								<?php while(have_rows ('menu-products', 'options' ) ) : the_row();
									$class = '';
									$link  = get_sub_field('link');
									if( $link['url'] == $active_page ) {
										$class = 'active';
									}
								?>
									<li>
										<a href="<?php echo $link['url']; ?>" class="btn rose <?php echo $class; ?>" title="<?php echo $link['title']; ?>" target="<?php echo $link['target']; ?>">
											<?php echo $link['title']; ?>
										</a>
									</li>
								<?php endwhile; ?>
								</ul>
							<?php endif;  ?>
							<?php if (have_rows ('menu-pages', 'options') ) : ?>
								<ul class="link-list inline page-links">
								<?php while(have_rows ('menu-pages', 'options' ) ) : the_row();
									$class = '';
									$link = get_sub_field('link');
									if( $link['url'] == $active_page ) {
										$class = 'active';
									}
								?>
									<li>
										<a href="<?php echo $link['url']; ?>" class="btn blue <?php echo $class; ?>" title="<?php echo $link['title']; ?>" target="<?php echo $link['target']; ?>">
											<?php echo $link['title']; ?>
										</a>
									</li>
								<?php endwhile; ?>
								</ul>
							<?php endif;  ?>
						</div>
						<?php if ( $buynow_btn ) : ?>
						<div class="col-lg-4 col-xl-3 buynow-link">
							<a href="<?php echo $buynow_btn['url']; ?>" class="btn-buynow" title="<?php echo $buynow_btn['title']; ?>" >
								<?php echo $buynow_label; ?>
							</a>
							<?php
							if ($lang) :
								$translations = pll_the_languages( array( 'raw' => 1 ) );
							?>
							<div class="lang">
								<span class="lang__btn">
									<?php echo pll_current_language(); ?>
								</span>
								<ul class="lang__switch">
									<?php foreach ( $translations as $lang ) : ?>
									<li class="lang__item">
										<a href="<?php echo $lang[ 'url' ] ?>" class="lang__link" title="<?php echo $lang[ 'name' ] ?>">
											<?php echo $lang[ 'name' ] ?>
										</a>
									</li>
									<?php endforeach; ?>
								</ul>
							</div>
							<?php endif; ?>
						</div>
						<?php endif; ?>
					</div>
				</nav>
			</div>
		</div>
	</div>
</header>

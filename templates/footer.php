<?php
	$retailers_title = get_field( 'retailers_title', 'options' );
	$legal           = get_field( 'legal', 'options' );
	$footer          = get_field( 'footer', 'options' );
	$logo_brand      = get_field( 'logo_generic', 'options' );
	$social          = get_field( 'social-media', 'options' );
	$brand_name      = get_field( 'brandname', 'options' );
	$brand_slogan    = get_field( 'brandslogan', 'options' );
	$top             = get_field( 'back_top', 'options' );
	$col_one         = get_field( 'footer-first-col', 'options' );
	$col_two         = get_field( 'footer-second-col', 'options' );
	$col_three       = get_field( 'footer-third-col', 'options' );

	$page_ref = get_field( 'reference' );
?>
<!-- Footer -->
<?php include( 'retailers.php' ); ?>
<footer class="footer">
	<!-- Second Footer Section -->
	<div class="footer-main">
		<div class="container">
			<div class="row d-flex flex-column-reverse flex-lg-row">
				<div class="footer-main--text col-12 col-lg-3">
					<a href="<?php bloginfo( 'url' ); ?>" title="<?php echo $brand_slogan; ?> ">
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/logo-lactacyd-pink.svg" class="logo" alt="<?php echo 'Logo Lactacyd'; ?>">
					</a>
					<p>
						<?php echo $footer['address']; ?>
					</p>
				</div>
				<div class="footer-main--menu col-lg-9">
					<div class="row">
						<?php if( have_rows( 'social-media', 'options' ) ) : ?>
							<div class="btn-social">
								<ul class="list-inline">
								<?php while ( have_rows( 'social-media', 'options' ) ) : the_row();
									$link = get_sub_field('social__url');
									$media = get_sub_field('social__name');
									$class = 'fa fa-' . $media;
								?>
									<li>
										<a href="<?php echo $link['url']; ?>" alt="<?php echo $brand_name . ' ' . $media; ?>" target="_blank">
											<i class="<?php echo $class; ?>"></i>
										</a>
									</li>
								<?php endwhile; ?>
								</ul>
							</div>
						<?php endif; ?>
					</div> <!-- end row links -->
				</div>
			</div> <!-- row -->
		</div>
	</div>
	<div class="footer-infos container">
		<div class="row">
			<div class="footer-infos--footnotes col-md-9 col-lg-10">
				<p>
					<?php echo $footer['copy']; ?>
				</p>
				<ul class="list-inline">
					<li class="list-inline-item">
						<a href="<?php echo $legal['cookies']['url']; ?>" target="_blank" title="<?php echo $legal['cookies']['title']; ?>">
							<?php echo $legal['cookies']['title']; ?>
						</a>
					</li>
					<li class="list-inline-item">
						<a href="<?php echo $legal['privacy']['url']; ?>" target="_blank" title="<?php echo $legal['privacy']['title']; ?>">
							<?php echo $legal['privacy']['title']; ?>
						</a>
					</li>
					<li class="list-inline-item">
						<a href="<?php echo $legal['terms']['url']; ?>" target="_blank" title="<?php echo $legal['terms']['title']; ?>">
							<?php echo $legal['terms']['title']; ?>
						</a>
					</li>
				</ul>
				<?php
					echo $footer['footnote'];
					if ( $page_ref ) :
						echo $page_ref;
					endif;
				?>

			</div>
			<div class="footer-infos--brand col-md-3 col-lg-2">
				<a href="#" title="Perrigo">
					<img src="/themes/lactacyd/assets/images/logo-perrigo.svg" class="logo-perrigo" alt="Perrigo">
				</a>
			</div>
		</div>
	</div>
</footer>

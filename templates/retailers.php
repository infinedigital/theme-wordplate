<?php $label = get_field( 'buynow', 'options' ); ?>
<?php if( have_rows( 'retailers', 'options' ) ): ?>
	<aside class="retailers white--bg">
		<div class="container">
			<div class="main-carousel-retailer flex-fill">
				<?php
				while ( have_rows( 'retailers', 'options' ) ) : the_row();
				$logo = get_sub_field( 'logo' );
				$link = get_sub_field( 'link' );
				$id   = get_sub_field( 'id' );
				?>
				<div class="retailers__item carousel-cell">
					<img src="<?php echo $logo['url']; ?>" class="logo img-fluid" alt="<?php echo $logo['title']; ?>">
					<a id="<?php echo $id; ?>" href="<?php echo $link['url']; ?>" class="btn-secondary small" title="<?php echo $link['title']; ?>" target="_blank">
						<?php echo $label; ?>
					</a>
				</div>
			<?php endwhile;  ?>
		</div>
	</div>
</aside>
<?php endif;
